-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 08 Décembre 2017 à 00:10
-- Version du serveur :  10.0.31-MariaDB-0ubuntu0.16.04.2
-- Version de PHP :  7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bot`
--

-- --------------------------------------------------------

--
-- Structure de la table `BOTCONJUG`
--

CREATE TABLE `BOTCONJUG` (
  `id` int(11) NOT NULL,
  `verbe` int(11) NOT NULL,
  `temps` int(11) NOT NULL,
  `conjug` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `BOTMESSAGE`
--

CREATE TABLE `BOTMESSAGE` (
  `id` int(11) NOT NULL,
  `msg` varchar(1000) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `BOTSUBJECT`
--

CREATE TABLE `BOTSUBJECT` (
  `id` bigint(20) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `BOTTIMES`
--

CREATE TABLE `BOTTIMES` (
  `id` int(11) NOT NULL,
  `name_time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `BOTVERBS`
--

CREATE TABLE `BOTVERBS` (
  `id` int(11) NOT NULL,
  `verb` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `BOTWORDS`
--

CREATE TABLE `BOTWORDS` (
  `id` int(11) NOT NULL,
  `word` varchar(255) NOT NULL,
  `subjectid` int(11) NOT NULL,
  `messageid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `BOTCONJUG`
--
ALTER TABLE `BOTCONJUG`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `conjug` (`conjug`);

--
-- Index pour la table `BOTMESSAGE`
--
ALTER TABLE `BOTMESSAGE`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `BOTSUBJECT`
--
ALTER TABLE `BOTSUBJECT`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `BOTTIMES`
--
ALTER TABLE `BOTTIMES`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_time` (`name_time`);

--
-- Index pour la table `BOTVERBS`
--
ALTER TABLE `BOTVERBS`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `verb` (`verb`);

--
-- Index pour la table `BOTWORDS`
--
ALTER TABLE `BOTWORDS`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `BOTCONJUG`
--
ALTER TABLE `BOTCONJUG`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `BOTMESSAGE`
--
ALTER TABLE `BOTMESSAGE`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `BOTSUBJECT`
--
ALTER TABLE `BOTSUBJECT`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `BOTTIMES`
--
ALTER TABLE `BOTTIMES`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `BOTVERBS`
--
ALTER TABLE `BOTVERBS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `BOTWORDS`
--
ALTER TABLE `BOTWORDS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
