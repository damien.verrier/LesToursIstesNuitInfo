<?php
    /// <INCLUDE AND PRECONFIG
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    header('Content-Type: text/plain');
    include(dirname(__FILE__)."/../utils/fonctions.php");
    include("simple_html_dom.php");
    /// INCLUDE AND PRECONFIG>

    /// <CLASSES
    abstract class TypesPhrases
    {
        const Inconnu = 0;
        const Question = 1;
        const Normal = 2;
    }

    abstract class TypesPlur
    {
        const Inconnu = 0;
        const Solo = 1;
        const Multiple = 2;
    }

    abstract class TypesSexe
    {
        const Inconnu = 0;
        const Homme = 1;
        const Femme = 2;
    }

    class Phrase {
        public static $pronoms = array('je','tu','il','elle','on','nous','vous','ils','elles');
        public static $pronomsPlur = array('nous','vous','ils','elles');
        public static $pronomsSing = array('je','tu','on','il','elle');
        public static $pronomsFem = array('elle','elles');
        public static $pronomsHom = array('il','ils','on','nous','vous');
        public static $conjonctionCoord = array(' et ',' ou ',' ni ',' mais ',' or ',' car ',' donc ');
        public static $conjonctionSubordination = array(' ainsi ',' aussi ',' cependant ',' comme ',' lorsque ',' néanmoins ',
        ' puisque ',' quand ',' que ',' quoique ',' si ',' soit ',' toutefois ');
        public static $conjonctionLocution = array(' à ce que ', ' à condition que ', ' afin que ',' ainsi que ',' alors que ',
        'à mesure que','à moins que');
        public $contenu = NULL;
        public $type = TypesPhrases::Inconnu;
        public $sujet = NULL;
        public $sujetPlur = TypesPlur::Inconnu;
        public $sujetSexe = TypesSexe::Inconnu;
        public $verbe = NULL;
        public $mots = NULL;

        function __construct($phrase) {
            $this->contenu = strtolower($phrase);
            $this->contenu = str_replace('|','',$this->contenu);
        }

        function estVide(){
            if($this->contenu === NULL)
                return true;
            else
                return false;
        }

        function supprimerConjonction()
        {
            $this->contenu = str_replace(Phrase::$conjonctionCoord,' ',$this->contenu);
            $this->contenu = str_replace(Phrase::$conjonctionSubordination,' ',$this->contenu);
            $this->contenu = str_replace(Phrase::$conjonctionLocution,' ',$this->contenu);
        }

        function simplifierPhrase()
        {
            $temp = str_replace(array('.','?','!',),'|',$this->contenu);
            $temp = explode('|',$temp)[0];
            $this->contenu = $temp;
        }

        function trouverSujet()
        {
            foreach(Phrase::$pronoms as $sujet)
                if(strpos($this->contenu, $sujet) !== false)
                {
                    $this->sujet = $sujet;
                }
            return false;
        }

        function trouverPlur()
        {
            if(in_array($this->sujet,Phrase::$pronomsPlur))
                $this->sujetPlur = TypesPlur::Multiple;
            else if (in_array($this->sujet,Phrase::$pronomsSing))
                $this->sujetPlur = TypesPlur::Solo;
            else
                $this->sujetPlur = TypesPlur::Inconnu;
        }

        function trouverSexe()
        {
            if(in_array($this->sujet,Phrase::$pronomsPlur))
                $this->sujetSexe = TypesSexe::Homme;
            else if (in_array($this->sujet,Phrase::$pronomsHom))
                $this->sujetSexe = TypesSexe::Femme;
            else
                $this->sujetSexe = TypesSexe::Inconnu;
        }

        function identifierSujet()
        {
            $this->supprimerConjonction();
            $this->simplifierPhrase();
            $this->trouverSujet();
            $this->trouverSexe();
            $this->trouverPlur();
        }
    }

    /// CLASSES>

    /// <FUNCTIONS
    /// FUNCTIONS>

    /// <MAIN
    try {
        $db = new PDO('mysql:host=localhost;dbname=bot', 'root', 'root');
        /*foreach($dbh->query('SELECT * from FOO') as $row) {
            print_r($row);
        }*/
        $test = new Phrase("Hier soir dans le métro, à l'heure de pointe, la personne assise à côté de moi a sorti un mac, l'a posé sur ses genoux.... et a lancé un jeu porno très explicite. Ça a duré sur presque toute la longueur de la ligne, d'un terminus à l'autre. Après avoir terminé un premier jeu, en 3D à peu près réaliste, il est passé sur un deuxième avec des graphismes genre comics américain.");
        $test->identifierSujet();
        //var_dump(file_get_contents('http://leconjugueur.lefigaro.fr/conjugaison/verbe/manger.html'));
        var_dump($test);
        $html = new simple_html_dom();
        $handle = fopen("verbs.txt", "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $verb = (str_replace("\n","",str_replace("VM1090:3 ","",$line)));
                $prepared = $db->prepare('INSERT IGNORE INTO BOTVERBS VALUES(NULL,?);');
                $prepared->execute(array($verb));
                $html->load_file('https://leconjugueur.lefigaro.fr/conjugaison/verbe/'.$verb.'.html');
                $list = $html->find('div[class=conjugBloc]');
                $delete = array('<b>','<p>','</b>','</p>',"j'","tu ","il ","nous ","vous ","ils ","je ");
                $present = 0;
                foreach($list as $elem)
                {
                    if(count($elem->children)>1)
                    {
                        $time = html_entity_decode(str_replace($delete,'',$elem->children[0]->children[0]))."\n";
                        $conjugs = explode("\n",html_entity_decode(str_replace('<br />',"\n",str_replace($delete,'',$elem->children[1]).'')));
                        $prepared = $db->prepare('INSERT IGNORE INTO BOTTIMES VALUES(NULL,?);');
                        $prepared->execute(array($time));
                        foreach($conjugs as $conjug)
                        {
                            $prepared = $db->prepare('INSERT IGNORE INTO BOTCONJUG VALUES(NULL,(SELECT id FROM BOTVERBS WHERE verb = ?),(SELECT id FROM BOTTIMES WHERE name_time = ?),?);');
                            $prepared->execute(array($verb,$time,$conjug));
                        }
                        //echo '';
                    }
                    //echo "\n";
                }
            }
            fclose($handle);
        } else {
            // error opening the file.
        } 

        //var_dump(getKeywords(cleanString(removeAnnoyingChars("J'aime le fromage !"))));
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
    // MAIN>
?>